from flask import Flask, render_template, request, session, redirect

app = Flask(__name__)
app.secret_key = "secret_key_here"
form_data = []

@app.route("/")
def home():
    return render_template("index.html")

@app.route("/forms", methods=["GET", "POST"])
def forms():
    if request.method == "POST":
        name = request.form["name"]
        email = request.form["email"]
        musica_favorita = request.form["musica_favorita"]
        cantor = request.form["cantor"]
        lancamento = request.form["lancamento"]
        form_data.append({
            "name": name,
            "email": email,
            "musica_favorita": musica_favorita,
            "cantor": cantor,
            "lancamento": lancamento
        })
        return redirect("/data")  
    return render_template("forms.html")

@app.route("/data")
def data():
    return render_template("data.html", form_data=form_data)

if __name__ == "__main__":
    app.run(debug=True)